#lang racket

(require 2htdp/universe)
(require 2htdp/image)
(require "StatefulWorld.rkt")
(require "util.rkt")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(provide Goofball%
         make-round-goofball
         make-stealth-goofball
         GOOFBALL-INITIAL-RADIUS)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; CONSTANTS
(define GOOFBALL-SELECT-CURSOR-IMAGE (circle 4 "solid" "orange"))

(define GOOFBALL-INITIAL-RADIUS (stealth-goofball-radius-at
                                  GOOFBALL-INITIAL-X
                                  GOOFBALL-INITIAL-Y))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; A Goofball is a
;   (new Goofball%
;     [x NonNegInt]
;     [y NonNegInt]
;     [vx Integer]
;     [vy Integer] 
;     [radius NonNegInt]
;     [stealth? Boolean]
;     [selected? Boolean]
;     [last-mx NonNegInt]
;     [last-my NonNegInt]
;     [w MyStatefulWorld<%>])
; INTERPRETATION:
;   x is the x-coordinate of the Goofball inside the canvas (in pixels)
;   y is the y-coordinate of the Goofball inside the canvas (in pixels)
;   vx is the x-component of the velocity of the Goofball (in pixels/tick)
;   vy is the y-component of the velocity of the Goofball (in pixels/tick)
;   radius refers to the
;     -- (length of the diagonal of square) / 2 if stealth? is true
;          ie. a stealth goofball
;     -- radius of the circle represented by round goofball when stealth? is
;          false
;   selected? refers to whether the goofball is selected with a mouse
;   stealth? whether it is stealthy or round goofball
;   last-mx is the position of the last mouse click x coordinate.
;   last-my is the position of the last mouse click y coordinate
;   w is the world of which this goofball is part of.
; WHERE:
;   0 <= x, last-mx <= 800
;   0 <= y, last-my <= 600

(define Goofball% 
  (class* object% (StatefulGoofball<%>)
    
    (init-field [x  GOOFBALL-INITIAL-X]
                [y  GOOFBALL-INITIAL-Y]
                [vx GOOFBALL-INITIAL-VX]
                [vy GOOFBALL-INITIAL-VY]
                [radius GOOFBALL-INITIAL-RADIUS]
                [stealth? GOOFBALL-INITIAL-STEALTH]
                [selected? GOOFBALL-INITIAL-SELECTED]
                [last-mx GOOFBALL-INITIAL-MX]
                [last-my GOOFBALL-INITIAL-MY]
                [w null])
    
    (super-new)
    
    ; -> Integer
    ; RETURNS: the x  coordinate of the center of this goofball
    (define/public (get-x) x)

    ; -> Integer
    ; RETURNS: the y  coordinate of the center of this goofball
    (define/public (get-y) y)
    
    ; -> Integer
    ; RETURNS: the vx  component of this goofball's velocity
    (define/public (get-vx) vx)

    ; -> Integer
    ; RETURNS: the vy  component of this goofball's velocity
    (define/public (get-vy) vy)
    
    ; -> Boolean
    ; RETURNS: true iff this goofball has the stealth of a Stealth Goofball
    (define/public (stealthy?) stealth?)

    ; -> Boolean
    ; GIVEN: no arguments
    ; RETURNS: whether the goofball is selected or not
    (define/public (for-test:selected?) selected?)
    
    ; -> Void
    ; GIVEN: no arguments
    ; EFFECT: updates this widget to its state following a tick
    ;     in which the world is paused
    (define/public (after-tick)
      (cond
        [(or selected? (send w world-paused?)) (void)]
        [else (goofball-after-tick)]))

    ;; -> Void
    ;; GIVEN: no arguments
    ;; EFFECT: updates this widget to its state following a tick
    ;;     in which the world is paused
    (define/public (after-tick-while-paused)
      (void))

    ; -> Void
    ; GIVEN: no arguments
    ; EFFECT: Updates the goofball to a state after tick
    (define (goofball-after-tick)
      (let* ((old-x x)
            (old-y y))
        (begin
          (set! x (constrained-x (+ old-x vx)))
          (set! y (constrained-y (+ old-y vy)))
          (set! vx
            (if (not (= (+ old-x vx) (constrained-x (+ vx old-x))))
              (* -1 vx)
              vx))
          (set! vy
            (if (not (= (+ old-y vy) (constrained-y (+ vy old-y))))
              (* -1 vy)
              vy))
         (set! radius
           (if stealth?
               (stealth-goofball-radius-at x y)
               (round-goofball-radius-at x y))))))
    
    ; Integer Integer -> MyStatefulWidget<%>
    ; GIVEN: the current world and the x and y coordinates of button down
    ; RETURNS: the state of this object that should follow the
    ; specified mouse event at the given location.
    (define/public (after-button-down mx my)
      (if (inside? mx my)
        (begin
          (set! selected? true)
          (set! last-mx mx)
          (set! last-my my))
        (void)))

    ; Integer Integer -> Void
    ; GIVEN: the current world and the x and y coordinates of button up
    ; EFFECT: the state of this object that should follow the
    ; specified mouse event at the given location.
    (define/public (after-button-up mx my)
      (if selected?
        (after-drag/button-up
          (x-from-last-mx last-mx mx)
          (y-from-last-my last-my my)
          false)
        (void)))

    ; Integer Integer -> Void
    ; GIVEN: the current world and the x and y coordinates at end of drag
    ; EFFECT: the state of this object that should follow the
    ; specified mouse event at the given location.
    (define/public (after-drag mx my)
      (if selected?
        (begin  
          (after-drag/button-up
            (x-from-last-mx last-mx mx)
            (y-from-last-my last-my my)
            true)
          (set! last-mx mx)
          (set! last-my my))
        (void)))

    ; Integer Integer Boolean -> Void
    ; GIVEN: the x and y coordinates and whether the goofball is selected or not
    ; EFFECT: updaates the radius and selected value
    (define (after-drag/button-up newx newy new-selected?)
      (begin
        (set! x newx)
        (set! y newy)
        (set! radius (radius-after-drag/button-down newx newy))
        (set! selected? new-selected?)))
    
    ; Integer Integer -> Integer
    ; GIVEN: the old and the current mouse x coordinates
    ; RETURNS: the new x coordinate of the center of the goofball
    (define (x-from-last-mx mx0 mx)
      (+ x (- mx mx0)))

    ; Integer Integer -> Integer
    ; GIVEN: the old and the current mouse y coordinates
    ; RETURNS: the new y coordinate of the center of the goofball
    (define (y-from-last-my my0 my)
      (+ y (- my my0)))

    ; Integer Integer -> NonNegInt
    ; GIVEN: the x and y coordinate of the center of goofball
    ; RETURNS: the new radius of the goofball
    (define (radius-after-drag/button-down newx newy)
      (if stealth?
          (stealth-goofball-radius-at newx newy)  
          (round-goofball-radius-at newx newy))) 

    ; Integer Integer -> Boolean
    ; GIVEN: x and y coordinate
    ; RETURNS: whether the given x and y coordinate lie inside round goofball
    (define (inside-circle? mx my)
      (<=
       (+
        (sqr (- x mx))
        (sqr (- y my)))
       (sqr radius)))

    ; -> Real
    ; GIVEN: no arguments
    ; RETURNS: the length of side/2 of a stealth goofball
    (define (square-length/2) (/ radius SQRT2))

    ; -> Real
    ; GIVEN: no arguments
    ; RETURNS: the length of side of a stealth goofball
    (define (square-length) (* 2 (square-length/2)))

    ; -> Real
    ; GIVEN: no arguments
    ; RETURNS: the length of side of a stealth goofball
    (define/public (for-test:square-length) (square-length))

    ; Integer Integer -> Boolean
    ; GIVEN: x and y coordinates
    ; RETURNS: true if the given coordinates lie inside the stealth goofball
    (define (inside-square? mx my)
      (let* ((top-left-x       (- x (square-length/2)))
             (top-left-y       (- y (square-length/2)))
             (bottom-right-x   (+ x (square-length/2)))
             (bottom-right-y   (+ y (square-length/2))))
        (and
         (>= mx top-left-x)
         (<= mx bottom-right-x)
         (>= my top-left-y)
         (<= my bottom-right-y))))

    ; Integer Integer -> Boolean
    ; GIVEN: x and y coordinates
    ; RETURNS: true if the coordinates lie inside the goofball, false otherwise
    (define (inside? mx my)
      (if stealth?
          (inside-square? mx my)  
          (inside-circle? mx my)))

    ; -> Image
    ; GIVEN: no arguments
    ; RETURNS: the image of stealth goofball
    (define (stealth-image)
      (square (* SQRT2 radius) "outline" (color)))

    ; -> Image
    ; GIVEN: no arguments
    ; RETURNS: the image of round goofball
    (define (round-image)
      (circle radius "outline" (color)))

    ; -> Image
    ; GIVEN: no arguments
    ; RETURNS: the image of velocity text
    (define (velocity-image)
      (if (and (not selected?) (max-radius?))
        (text
          (string-append
            "("
            (number->string vx)
            ","
            (number->string vy)
            ")")
          12
          (if stealth? "SlateGray" "Blue"))  
        empty-image))

    ; -> Boolean
    ; GIVEN: no arguments
    ; RETURNS: true if goofball is not at a contracted state, false otherwise
    (define (max-radius?)
      (if stealth?
          (= STEALTH-GOOFBALL-DIAGONAL/2-LENGTH radius)  
          (= ROUND-GOOFBALL-MAX-RADIUS radius)))

    ; -> Real
    ; GIVEN: no arguments
    ; RETURNS: true if goofball is not at a contracted state, false otherwise
    (define/public (for-test:max-radius?) (max-radius?))
    
    ; -> Real
    ; RETURNS: radius of the underlying goofball.
    (define/public (get-radius)
      radius)
    
    ;;; after-key-event : KeyEvent -> Widget
    ;;; GIVEN: a key event and a time
    ;;; RETURNS: the state of this object that should follow the
    ;;; given key event
    (define/public (after-key-event kev)
      (if selected?
          (cond
            [(key=? kev "left")  (begin (set! vx (- vx 1)))]
            [(key=? kev "right") (begin (set! vx (+ vx 1)))]
            [(key=? kev "up")    (begin (set! vy (- vy 1)))]
            [(key=? kev "down")  (begin (set! vy (+ vy 1)))]
            [else (void)])  
          (void)))

    
    ;;; Scene WorldState -> Scene
    ;;; GIVEN: a scene
    ;;; RETURNS: a scene like the given one, but with this object
    ;;; painted on it.
    (define/public (add-to-scene scene)
      (cursor-image
       (place-image
        (overlay
         (image)
         (velocity-image))
        x
        y
        scene)
       last-mx
       last-my))

    ; Scene NonNegInt NonNegInt -> Scene
    ; GIVEN: the x and y coordinate where the selected cursor should be placed
    ; RETURNS: the scene after placing the cursor on the scene
    (define (cursor-image scene x y)
      (if selected?
          (place-image GOOFBALL-SELECT-CURSOR-IMAGE x y scene)
          scene))

    ; -> ImageColor
    ; GIVEN: no arguments
    ; RETURNS: the color of the goofball
    (define (color)
      (cond
        [selected? "Green"]
        [(not (max-radius?)) "Red"]
        [stealth? "SlateGray"]
        [else "Blue"]))

    ; -> Image
    ; GIVEN: no arguments
    ; RETURNS: the image of the goofball
    (define (image)
      (if stealth?
          (stealth-image)
          (round-image)))

    ; -> Goofball%
    ; GIVEN: No arguments
    ; RETURNS: a clones the underlying fields for creating new goofball
    (define/public (clone)
      (new Goofball%
        [x x] [y y]
        [vx vx] [vy vy]
        [radius radius]
        [stealth? stealth?]
        [selected? selected?]
        [last-mx last-mx] [last-my last-my]
        [w w]))

    ))

;;; make-stealth-goofball : Integer Integer Integer Integer -> Goofball<%>
;;; GIVEN: x and y coordinates and velocity components vx and vy
;;; RETURNS: A Stealth Goofball with its center at the x and y coordinates
;;;     and velocity components vx and vy
;;; EXAMPLES: See test cases
;;; STRATEGY: Use constructor template of Goofball%
(define (make-stealth-goofball x y vx vy)
  (new Goofball%
       [x x]
       [y y]
       [vx vx]
       [vy vy]
       [radius (stealth-goofball-radius-at x y)]
       [stealth? true]
       [selected? false]))

;;; make-round-goofball : Integer Integer Integer Integer -> Goofball<%>
;;; GIVEN: x and y coordinates and velocity components vx and vy
;;; RETURNS: A Round Goofball with its center at the x and y coordinates
;;;     and velocity components vx and vy
;;; EXAMPLES: See test cases
;;; STRATEGY: Use constructor template of Goofball%
(define (make-round-goofball x y vx vy)
  (new Goofball%
       [x x]
       [y y]
       [vx vx]
       [vy vy]
       [radius (round-goofball-radius-at x y)]
       [stealth? false]
       [selected? false]))
