#lang racket

(require 2htdp/universe)
(require 2htdp/image)

(require "StatefulWorld.rkt")
(require "goofball.rkt")
(require "util.rkt")

(provide RoundGoofball%
         make-round-goofball)

; A RoundGoofball% is a
;   (new RoundGoofball%
;     [x NonNegInt]
;     [y NonNegInt]
;     [vx Integer]
;     [vy Integer] 
;     [radius NonNegInt]
;     [selected? Boolean]
;     [last-mx NonNegInt]
;     [last-my NonNegInt]
;     [w MyStatefulWorld<%>])
; INTERPRETATION:
;   x is the x-coordinate of the Goofball inside the canvas (in pixels)
;   y is the y-coordinate of the Goofball inside the canvas (in pixels)
;   vx is the x-component of the velocity of the Goofball (in pixels/tick)
;   vy is the y-component of the velocity of the Goofball (in pixels/tick)
;   radius refers to the radius of the circle represented by round goofball 
;   selected? refers to whether the goofball is selected with a mouse
;   last-mx is the position of the last mouse click x coordinate.
;   last-my is the position of the last mouse click y coordinate
;   w is the world of which this goofball is part of.
; WHERE:
;   0 <= x, last-mx <= 800
;   0 <= y, last-my <= 600

(define RoundGoofball% 
  (class* Goofball% (StatefulGoofball<%>)

    (inherit-field x y vx vy radius selected? last-mx last-my w)

    (super-new)
    
    (set-field! x this GOOFBALL-INITIAL-X)
    (set-field! y this GOOFBALL-INITIAL-Y)
    (set-field! vx this GOOFBALL-INITIAL-VX)
    (set-field! vy this GOOFBALL-INITIAL-VY)
    (set-field! radius this ROUND-GOOFBALL-MAX-RADIUS)
    (set-field! selected? this GOOFBALL-INITIAL-SELECTED)
    (set-field! last-mx this GOOFBALL-INITIAL-MX)
    (set-field! last-my this GOOFBALL-INITIAL-MY)
    (set-field! w this null)
    
    ; Integer Integer -> NonNegInt
    ; GIVEN: the center coordinates of a goofball
    ; RETURNS: the radius of the goofball at x & y
    (define/override (goofball-radius-at x y)
      (round-goofball-radius-at x y))

    ; -> Image
    ; GIVEN: no arguments
    ; RETURNS: the image of round goofball
    (define/override (image)
      (circle radius "outline" (color)))

    ; -> Image
    ; GIVEN: no arguments
    ; RETURNS: the image of round goofball
    (define (color)
      (cond
        [selected? "Green"]
        [(not (max-radius?)) "Red"]
        [else "Blue"]))
    
    ; -> Image
    ; GIVEN: no arguments
    ; RETURNS: the image of velocity text
    (define/override (velocity-image)
      (if (and (not selected?) (max-radius?))
        (text
          (string-append
            "("
            (number->string vx)
            ","
            (number->string vy)
            ")")
          12
          "Blue")  
        empty-image))

    ; Integer Integer -> Boolean
    ; GIVEN: the x and y coordinates of the mouse event
    ; RETURNS: true if the mouse event happens inside the goofball, false
    ;   otherwise
    (define/override (inside? mx my)
      (<=
       (+
        (sqr (- x mx))
        (sqr (- y my)))
       (sqr radius)))

    ; -> Boolean
    ; GIVEN: no arguments
    ; RETURNS: true if goofball is not in a contracted state
    (define (max-radius?)
      (= ROUND-GOOFBALL-MAX-RADIUS radius))

    ; -> Boolean
    ; RETURNS: true iff this goofball has the stealth of a Stealth Goofball
    (define/override (stealthy?) false)

    ; -> Real
    ; GIVEN: no arguments
    ; RETURNS: true if goofball is not at a contracted state, false otherwise
    (define/public (for-test:max-radius?) (max-radius?))

    ; -> StealthGoofball%
    ; GIVEN: No arguments
    ; RETURNS: a clones the underlying fields for creating new goofball
    (define/override (clone)
      (let ((s (new RoundGoofball%)))
        (begin
          (set-field! x s x)
          (set-field! y s y)
          (set-field! vx s vx)
          (set-field! vy s vy)
          (set-field! radius s (round-goofball-radius-at x y))
          (set-field! selected? s selected?)
          (set-field! last-mx s last-mx)
          (set-field! last-my s last-my)
          (set-field! w s w)
          s)))
    
    ))

;;; make-round-goofball : Integer Integer Integer Integer -> Goofball<%>
;;; GIVEN: x and y coordinates and velocity components vx and vy
;;; RETURNS: A Round Goofball with its center at the x and y coordinates
;;;     and velocity components vx and vy
;;; EXAMPLES: See test cases
;;; STRATEGY: Use constructor template of Goofball%
(define (make-round-goofball x y vx vy)
  (let ((s (new RoundGoofball%)))
    (begin
       (set-field! x s x)
       (set-field! y s y)
       (set-field! vx s vx)
       (set-field! vy s vy)
       (set-field! radius s (round-goofball-radius-at x y))
       s)))
