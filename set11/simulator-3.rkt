#lang racket

(require rackunit)
(require 2htdp/universe)
(require "extras.rkt")

(require "StatefulWorld.rkt")
(require "goofball.rkt")
(require "robot.rkt")
(require "sagr.rkt")
(require "dagr.rkt")
(require "util.rkt")
(require "world.rkt")
(require "expl.rkt")
(require "stealth-goofball.rkt")
(require "round-goofball.rkt")

(provide run-world
         run
         make-world
         world-after-tick
         world-after-key-event
         world-after-mouse-event
         make-round-goofball
         make-stealth-goofball
         make-SAGR
         make-DAGR
         SimulatorState<%>
         Goofball<%>
         Robot<%>)

(check-location "11" "simulator-3.rkt")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; run-world : PosInt SimulatorState<%> -> SimulatorState<%>
;;; GIVEN: a frame rate (in seconds/tick) and a SimulatorState<%>
;;; WHERE: the SimulatorState<%> was returned by one of these provided functions
;;; RETURNS: the final state of the world after running the given world
;;; STRATEGY : Use template for big bang
(define (run-world rate world)
  (big-bang world
            (on-tick
             (lambda (w) (begin (send w after-tick) w))
             rate)
            (on-draw
             (lambda (w) (send w to-scene)))
            (on-key
             (lambda (w kev)
               (begin (send w after-key-event kev) w)))
            (on-mouse
             (lambda (w mx my mev)
               (begin
                 (send w after-mouse-event mx my mev)
                 w)))))
            
; run : PosReal -> World
; GIVEN: a frame rate, in secs/tick
; EFFECT: runs an initial world at the given frame rate
; RETURNS: the final state of the world
;;; STRATEGY : Call a more general function.
(define (run rate)
  (run-world rate (initial-world)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Goofball
; TESTS
; test-new-round-gb : Integer Integer Integer Ineger Integer Boolean
;  StatefulWorld<%> -> RoundGoofball%
; GIVEN: all the parameters required to create a round goofball
;   x,y - x & y coordinates
;   vx,vy - velocity coordinates
;   s? - whether it was selected
;   w - world
; RETURNS: the newly contructed round goofball
; STRATEGY: Use constructor template of RoundGoofball%
(define (test-new-round-gb x y vx vy s? w)
  (let ((s (new RoundGoofball%)))
  (begin
    (set-field! x s x)
    (set-field! y s y)
    (set-field! vx s vx)
    (set-field! vy s vy)
    (set-field! radius s (round-goofball-radius-at x y))
    (set-field! selected? s s?)
    (set-field! w s w)
    s)))

; test-new-round-gb : Integer Integer Integer Ineger Integer Boolean
;  StatefulWorld<%> -> StealthGoofball%
; GIVEN: all the parameters required to create a round goofball
;   x,y - x & y coordinates
;   vx,vy - velocity coordinates
;   s? - whether it was selected
;   w - world
; RETURNS: the newly contructed stealth goofball
; STRATEGY: Use constructor template of StealthGoofball%
(define (test-new-stealth-gb x y vx vy s? w)
  (let ((s (new StealthGoofball%)))
  (begin
    (set-field! x s x)
    (set-field! y s y)
    (set-field! vx s vx)
    (set-field! vy s vy)
    (set-field! radius s (stealth-goofball-radius-at x y))
    (set-field! selected? s s?)
    (set-field! w s w)
    s)))
  

(define G-R-1 (test-new-round-gb 50 75 5 3 false (initial-world)))

(define G-R-1-selected (test-new-round-gb 50 75 5 3 true (initial-world)))

(define G-R-2 (test-new-round-gb 55 78 5 3 false (initial-world)))

(define G-R-3 (test-new-round-gb 10 10 -12 -12 false (initial-world)))

(define G-R-3-after-tick (test-new-round-gb 2 2 12 12 false (initial-world)))

(define G-R-5 (test-new-round-gb 10 10 -12 0 false (initial-world)))

(define G-R-5-selected (test-new-round-gb 10 10 -12 0 true (initial-world)))

(define G-R-6 (test-new-round-gb 10 11 -12 0 false (initial-world)))

(define G-S-3 (test-new-stealth-gb 10 10 -12 -12 false (initial-world)))

(define G-S-3-after-tick (test-new-stealth-gb 2 2 12 12 false (initial-world)))

(define G-S-1 (test-new-stealth-gb 55 78 0 0 false (initial-world)))

(define G-S-1-selected (test-new-stealth-gb 55 78 0 0 true (initial-world)))

(define G-S-2 (test-new-stealth-gb 785 542 15 -10 true (initial-world)))

(define (G-W-1)
  (let ((world (make-world (list G-R-1 G-R-2 G-S-1) empty)))
    (begin
      (set-field! paused? world false)
      world)))

(define (G-W-1-paused)
  (let ((world (make-world (list G-R-1 G-R-2 G-S-1) empty)))
    world))

                                     

(define (test-gclone g)
  (send g clone))

(define (test-after-tick g world)
  (let ((g-clone (test-gclone g)))
  (begin
    (if (field-bound? w g-clone)
      (set-field! w g-clone world)
      (void))
    (send g-clone after-tick)
     g-clone)))

(define (test-after-key-event g world ke)
  (let ((g-clone (test-gclone g)))
  (begin
    (if (field-bound? w g-clone)
      (set-field! w g-clone world)
      (void))
    (send g-clone after-key-event ke)
     g-clone)))

(define (test-after-mouse-button-down g world x y)
  (let ((g-clone (test-gclone g)))
  (begin
    (if (field-bound? w g-clone)
      (set-field! w g-clone world)
      (void))
    (send g-clone after-button-down x y)
     g-clone)))

(define (test-after-mouse-button-up g world x y)
  (let ((g-clone (test-gclone g)))
  (begin
    (if (field-bound? w g-clone)
      (set-field! w g-clone world)
      (void))
    (send g-clone after-button-up x y)
     g-clone)))

(define (test-after-mouse-button-drag g world x y)
  (let ((g-clone (test-gclone g)))
  (begin
    (if (field-bound? w g-clone)
      (set-field! w g-clone world)
      (void))
    (send g-clone after-drag x y)
     g-clone)))

(define (inject-last-mxy g mx my)
  (begin
    (set-field! last-mx g mx)
    (set-field! last-my g my)
    g))


; GoofballWidget<%> GoofballWidget<%> -> Void
; GIVEN: 2 goofballs2 
; EFFECT: Checks whether the centers are equal, else fails with error
(define (check-goofball-centers-equal? g1 g2)
  (and
   (check-equal? (send g1 get-x) (send g2 get-x))
   (check-equal? (send g1 get-y) (send g2 get-y))))

; GoofballWidget<%> GoofballWidget<%> -> Boolean
; GIVEN: 2 goofballs
; RETURNS: true if g1 and g2 are exactly same in all their fields
(define (check-goofball-equal? g1 g2)
   (and
    (= (send g1 get-x) (send g2 get-x))
    (= (send g1 get-y) (send g2 get-y))
    (= (send g1 get-vx) (send g2 get-vx))
    (= (send g1 get-vy) (send g2 get-vy))
    (boolean=? (send g1 stealthy?) (send g2 stealthy?))
    (<= (abs (- (send g1 get-radius) (send g2 get-radius))) 1e-4)
    (boolean=? (send g1 for-test:selected?) (send g2 for-test:selected?))))


(begin-for-test
  ; x & y
  (check-equal? (send G-R-1 get-x) 50)
  (check-equal? (send G-R-1 get-y) 75)

  ; vx & vy
  (check-equal? (send G-R-1 get-vx) 5)
  (check-equal? (send G-R-1 get-vy) 3)
  
  (check-equal? (send G-S-1 get-vx) 0)
  (check-equal? (send G-S-1 get-vy) 0)

  (check-equal? (send G-R-1 get-radius) 20)
  (check-equal? (send G-S-2 get-radius) (* SQRT2 15))

  (check-equal? (send G-R-1 stealthy?) false)
  (check-equal? (send G-S-1 stealthy?) true)

  (check-goofball-equal? (test-after-tick G-R-1-selected (G-W-1))
                         G-R-1-selected)
  
  (check-goofball-equal? (test-after-tick G-R-1 (G-W-1-paused)) G-R-1)
  
  (check-true
   (check-goofball-equal? (test-after-tick G-R-3  (G-W-1)) G-R-3-after-tick)
                          "G-R-3 after tick")

  (check-true
   (check-goofball-equal? (test-after-tick G-S-3 (G-W-1)) G-S-3-after-tick)
                          "G-S-3 after tick")

  (check-goofball-centers-equal? (test-after-tick G-R-1  (G-W-1)) G-R-2)

  (check-goofball-centers-equal? (test-after-tick G-S-1  (G-W-1)) G-S-1)

  (check-true
    (check-goofball-equal?
     (test-after-key-event G-S-1-selected (initial-world) "left")
     (test-new-stealth-gb 55 78 -1  0 true (initial-world))))
          

  (check-true
   (check-goofball-equal?
    (test-after-key-event G-S-1-selected (initial-world) "right")
    (test-new-stealth-gb 55 78 1 0 true (initial-world))))
        

  (check-true
   (check-goofball-equal?
    (test-after-key-event G-S-1-selected (initial-world) "up")
    (test-new-stealth-gb 55 78 0 -1 true (initial-world))))

  (check-true
   (check-goofball-equal?
    (test-after-key-event G-S-1-selected (initial-world) "down")
    (test-new-stealth-gb 55 78 0 1 true (initial-world))))

  (check-true
   (check-goofball-equal?
    (test-after-key-event G-S-1-selected (initial-world) "Q")
    G-S-1-selected))

  (check-true
   (check-goofball-equal?
    (test-after-key-event G-S-1 (initial-world) "Q")
    G-S-1))

  (check-true
   (check-goofball-equal?
    (test-after-key-event G-R-1-selected (initial-world) "left")
    (test-new-round-gb 50 75 4 3 true (initial-world))))

  (check-true
   (check-goofball-equal?
    (test-after-key-event G-R-1-selected (initial-world) "right")
    (test-new-round-gb 50 75 6 3 true (initial-world))))

  (check-true
   (check-goofball-equal?
    (test-after-key-event G-R-1-selected (initial-world) "up")
    (test-new-round-gb 50 75 5 2 true (initial-world))))

  (check-true
   (check-goofball-equal?
    (test-after-key-event G-R-1-selected (initial-world) "down")
    (test-new-round-gb 50 75 5 4 true (initial-world))))

  (check-true
   (check-goofball-equal?
    (test-after-mouse-button-down G-R-5 (initial-world) 10 9)
    (test-new-round-gb  10 10 -12 0 true (initial-world))))

  (check-true
   (check-goofball-equal?
    (test-after-mouse-button-down G-R-5 (initial-world) 800 600)
    G-R-5))
  
  (check-true
   (check-goofball-equal?
    (test-after-mouse-button-drag (inject-last-mxy (test-gclone G-R-5-selected)
                                                   10 8)
                                  (initial-world) 10 9)
                                  
   (test-new-round-gb 10 11 -12 0 true (initial-world))))

  (check-true
   (check-goofball-equal?
   (test-after-mouse-button-drag G-R-5 (initial-world) 800 600)
   G-R-5))

  (check-true
   (check-goofball-equal?
     (test-after-mouse-button-up (inject-last-mxy (test-gclone G-R-5-selected)
                                                  10 8)
                                 (initial-world)
                                 10 9)
     G-R-6))

  (check-true
   (check-goofball-equal?
   (test-after-mouse-button-up (inject-last-mxy (test-gclone G-R-5) 10 8)
                               (initial-world)
                               800 600)
   G-R-5))

;  ;;
;  (check-true
;   (check-goofball-equal?
;    (send (make-stealth-goofball 10 10 -12 0) after-button-down (initial-world)
;          10 9)
;   (new Goofball% [x 10] [y 10] [vx -12] [vy 0] [selected? true]
;        [stealth? true] [radius (stealth-goofball-radius-at 10 10)])))

;  (check-true
;   (check-goofball-equal?
;    (send (new Goofball% [x 10] [y 10] [vx -12] [vy 0] [selected? true]
;        [stealth? true] [radius (stealth-goofball-radius-at 10 10)])
;          after-drag
;          (new World% [gballs empty] [bots empty] [saved-mx 10] [saved-my 9])
;          10 8)
;    (new Goofball% [x 10] [y 9] [vx -12] [vy 0] [selected? true]
;        [stealth? true] [radius (stealth-goofball-radius-at 10 9)])))

  (check-= (send (make-stealth-goofball 50 75 5 3) for-test:square-length)
            35
           1e-6)
  (check-true (send (make-stealth-goofball 50 75 5 3) for-test:max-radius?))
  (check-true (send (make-round-goofball 50 75 5 3) for-test:max-radius?))) 


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Tests - SAGR:
(define test-sagr (new SAGR% [x 10] [y 10] [vx 2] [vy 2] [fuel 400]
                                 [w (new World%
                                         [gballs (list (make-round-goofball
                                                        50 50 1 1))]
                                         [bots '()]
                                         [paused? #f])]))

(begin-for-test
  (check-equal? (send (make-SAGR 10 10 2 2) get-x) 10 "X coordinate is proper")
  (check-equal? (send (make-SAGR 10 10 2 2) get-y) 10 "Y coordinate is proper")
  (check-equal? (send (make-SAGR 10 10 2 2) get-vx) 2 "vx is proper")
  (check-equal? (send (make-SAGR 10 10 2 2) get-vy) 2 "vy is proper")
  (check-equal? (send (make-SAGR 10 10 2 2) smart?) #t "Is smart")
  (check-equal? (send (make-SAGR 10 10 2 2) for-test:fuel) 400 "Fuel")
  (check-equal? (send (make-SAGR 10 10 2 2) for-test:dead?) #f "Not dead")
  (check-equal? (send (make-SAGR 10 10 2 2) alive?) #t "Not dead")

  (check-equal? (send (test-after-key-event
                        (new SAGR% [x 10] [y 10] [vx 2] [vy 2] [fuel 400])
                        (new World%
                             [gballs (list (make-round-goofball
                                            50 50 1 1))]
                             [bots '()]
                             [paused? #f])
                             "n") get-x)
                10 "Same object")
  
  
  (check-equal?  (send
                   (test-after-mouse-button-down
                     test-sagr
                     (initial-world)
                     2
                     2)
                get-x)
                10 "Same object")
  
  (check-equal? (send (test-after-mouse-button-up
                       (make-SAGR 10 10 2 2) (initial-world)  2 2)
                      get-x)
                10 "Same object")
  (check-equal? (send (test-after-mouse-button-drag
                       (make-SAGR 10 10 2 2)
                       (initial-world)  2 2) get-x)
                10 "Same object")
  (check-equal? (send (test-after-tick (make-SAGR 10 10 2 2)
                                       (new World%
                                         [gballs (list (make-round-goofball
                                                        50 50 1 1))]
                                         [bots (list (make-SAGR 10 10 2 2))]
                                         [paused? #t])) get-x)
                10 "Same object")
  (check-equal? (send (test-after-tick
                       (new SAGR% [x 10] [y 10] [vx 2] [vy 2] [fuel 0])
                       (new World%
                            [gballs (list (make-round-goofball
                                           50 50 1 1))]
                            [bots '()]
                            [paused? #f])) for-test:fuel)
                0 "Same object with fuel reduced")
  (check-equal? (send (test-after-tick
                       (new SAGR% [x 10] [y 10] [vx 2] [vy 2] [fuel 400])
                       (new World%
                            [gballs (list (make-round-goofball
                                           50 50 1 1))]
                            [bots '()]
                            [paused? #f])) for-test:fuel)
                399 "Same object with fuel reduced")
  (check-equal? (send (test-after-tick
                       (new SAGR% [x 10] [y 10] [vx 2] [vy 2] [fuel 400])
                       (new World%
                            [gballs '()]
                            [bots '()]
                            [paused? #f])) get-x)
                12 "next tick ")
  (check-equal? (send (test-after-tick
                       (new SAGR% [x 10] [y 10] [vx 2] [vy 2] [fuel 400])
                       (new World%
                            [gballs (list (make-round-goofball
                                           50 50 1 1))]
                            [bots '()]
                            [paused? #f]))
                      get-x)
                18 "next tick with vx +8")
  (check-equal? (send (test-after-tick
                       (new SAGR% [x 100] [y 100] [vx 2] [vy 2] [fuel 400])
                       (new World%
                            [gballs (list (make-round-goofball
                                           50 50 1 1))]
                            [bots '()]
                            [paused? #f]))
                      get-x)
                92 "next tick with vx -8")
  (check-equal? (send (test-after-tick
                       (new SAGR% [x 100] [y 100] [vx 2] [vy 2] [fuel 400])
                       (new World%
                            [gballs (append
                                     (list (make-round-goofball 50 50 1 1))
                                     (list (make-round-goofball 100 100 1 1)))]
                            [bots '()]
                            [paused? #f]))
                      get-x)
                100 "next tick with vx 0")
  (check-equal? (send (test-after-tick
                       (new SAGR% [x 100] [y 100] [vx 2] [vy 2] [fuel 400])
                       (new World%
                            [gballs (append
                                     (list (make-round-goofball 50 50 1 1))
                                     (list
                                      (make-stealth-goofball 120 120 1 1))
                                     (list (make-round-goofball 250 250 1 1)))]
                            [bots '()]
                            [paused? #f]))
                      get-x)
                108 "next tick with vx 8"))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Tests - DAGR:
(begin-for-test
  (check-equal? (send (make-DAGR 10 10 2 2) get-x) 10 "X coordinate is proper")
  (check-equal? (send (make-DAGR 10 10 2 2) get-y) 10 "Y coordinate is proper")
  (check-equal? (send (make-DAGR 10 10 2 2) get-vx) 2 "vx is proper")
  (check-equal? (send (make-DAGR 10 10 2 2) get-vy) 2 "vy is proper")
  (check-equal? (send (make-DAGR 10 10 2 2) smart?) #f "Dumb robot")
  (check-equal? (send (make-DAGR 10 10 2 2) for-test:dead?) #f "Not dead")
  (check-equal? (send (make-DAGR 10 10 2 2) alive?) #t "Not dead")
  
  (check-equal? (send (test-after-key-event
                       (make-DAGR 10 10 2 2)
                       (initial-world)
                       "n") get-x)
                10 "Same object")
  (check-equal? (send (test-after-mouse-button-down
                       (make-DAGR 10 10 2 2) (initial-world) 2 2)
                      get-x)
                10 "Same object")
  (check-equal? (send (test-after-mouse-button-up
                       (make-DAGR 10 10 2 2) (initial-world) 2 2)
                      get-x)
                10 "Same object")
  (check-equal? (send (test-after-mouse-button-drag
                       (make-DAGR 10 10 2 2) (initial-world) 2 2) get-x)
                10 "Same object")
  (check-equal? (send (test-after-tick
                       (make-DAGR 10 10 2 2)
                       (new World%
                            [gballs (append
                                     (list (make-round-goofball 50 50 1 1))
                                     (list (make-round-goofball 100 100 1 1)))]
                            [bots '()]
                            [paused? #t])) get-x)
                10 "Same object")
  (check-equal? (send (test-after-tick
                       (make-DAGR 10 10 2 2)
                       (new World%
                            [gballs (append
                                     (list (make-round-goofball 50 50 1 1))
                                     (list (make-round-goofball 100 100 1 1)))]
                            [bots '()]
                            [paused? #f]))
                      get-x)
                12 "next tick with vx +2"))

(begin-for-test
  (check-equal? (send (new Explosion% [x 10] [y 10]) get-ticks) 15 "Max ticks")
  (check-equal? (send (new Explosion% [x 10] [y 10]) for-test:get-x) 10 "x")
  (check-equal? (send (new Explosion% [x 10] [y 10]) for-test:get-y) 10 "y")
  (check-equal? (send (test-after-key-event
                       (new Explosion% [x 10] [y 10])
                       (initial-world) "s")
                      for-test:get-x) 10 "same")
  (check-equal? (send (test-after-mouse-button-down
                       (new Explosion% [x 10] [y 10]) 
                       (initial-world) 10 10) for-test:get-x) 10 "same")
  (check-equal? (send (test-after-mouse-button-up
                       (new Explosion% [x 10] [y 10]) 
                      (initial-world) 10 10) for-test:get-x) 10 "same")
  (check-equal? (send (test-after-mouse-button-drag
                       (new Explosion% [x 10] [y 10]) 
                      (initial-world) 10 10) for-test:get-x) 10 "same"))
